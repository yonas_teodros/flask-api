from models import Base,User,Scene,Files
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import exists

engine = create_engine('sqlite:///users.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

user1=User(name='Yonas')
user2=User(name='Federico')
user3=User(name='Andria')
session.add(user1)
session.add(user2)
session.add(user3)
session.commit()
scene_one=Scene(name='Horse',owner=user1)
scene_two=Scene(name='cat',owner=user1)
scene_three=Scene(name='Dog',owner=user2)
scene_four=Scene(name='Dog',owner=user3)

session.add(scene_one)
session.add(scene_two)
session.add(scene_three)
session.add(scene_four)
session.commit()


file_one=Files(name='pic_one',sub_owner=scene_one)
file_two=Files(name='pic_two',sub_owner=scene_one)
file_three=Files(name='pic_thre',sub_owner=scene_one)
file_four=Files(name='pic_four',sub_owner=scene_one)
session.add(file_one)
session.add(file_two)
session.add(file_three)
session.add(file_four)
session.commit()

u = session.query(User).filter_by(name = 'Yonas').first()
for i in u.scenes.all():
	print(i.name)

s = session.query(Scene).filter_by(name = 'Horse').first()
for j in s.files.all():
 	print(j.name)