import httplib2
import requests
import json
import sys

print "Running Endpoint Tester....\n"
address = raw_input("Please enter the address of the server you want to access, \n If left blank the connection will be set to 'http://localhost:5000':   ")
if address == '':
	address = 'http://localhost:5000'

###########################################################################
##                                                                       ##
##                        User                                           ##
###########################################################################

#Making a POST Request
print "Making a POST request to /users..."
try:
	url = address + '/users?name=tomas&description=the+first+test'
	h = httplib2.Http()
	resp, result = h.request(url, 'POST')
	obj = json.loads(result)
	ID  = obj['user']['id']
	Name =obj['user']['name']
	print Name , ID
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])

except Exception as err:
	print "Test 1 FAILED: Could not make POST Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 1 PASS: Succesfully Made POST Request to /Users"




#Making a GET Request
print "Making a GET Request for /Users..."
try:
	url = address + "/users"
	h = httplib2.Http()
	resp, result = h.request(url, 'GET')
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])
except Exception as err:
	print "Test 2 FAILED: Could not make GET Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 2 PASS: Succesfully Made GET Request to /users"




#Making GET Requests to /users/id
print "Making GET requests to /users/id "

try:
	id = 1
	url = address + "/users/%s" % id 
	h = httplib2.Http()
	resp, result = h.request(url, 'GET')
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])
except Exception as err:
	print "Test 3 FAILED: Could not make GET Requests to web server"
	print err.args
	sys.exit()
else:
	print "Test 3 PASS: Succesfully Made GET Request to /users/id"



#Making a PUT Request
print "Making PUT requests to /users/id "

try:
	id = 2 

	url = address + "/users/%s?name=alex&description=the+second+test" % id 
	h = httplib2.Http()
	resp, result = h.request(url, 'PUT')
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])

except Exception as err:
	print "Test 4 FAILED: Could not make PUT Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 4 PASS: Succesfully Made PUT Request to /users/id"


#Making a DELETE Request

# print "Making DELETE requests to /users/id ... "

# try:
# 	id = 4
# 	url = address + "/users/%s" % id 
# 	h = httplib2.Http()
# 	resp, result = h.request(url, 'DELETE')
# 	if resp['status'] != '200':
# 		raise Exception('Received an unsuccessful status code of %s' % resp['status'])
	

# except Exception as err:
# 	print "Test 5 FAILED: Could not make DELETE Requests to web server"
# 	print err.args
# 	sys.exit()
# else:
# 	print "Test 5 PASS: Succesfully Made DELETE Request to /users/id"
# 	print "ALL TESTS PASSED!!"

###########################################################################
##                                                                       ##
##                        Scene                                          ##
###########################################################################

#Making a POST Request
print "Making a POST request to /Scene..."
try:
	url = address + "/scene?name=yonas&scene_name=dog"
	h = httplib2.Http()
	resp, result = h.request(url, 'POST')
	obj = json.loads(result)
	ID  = obj['sce']['id']
	Name =obj['sce']['name']
	print Name , ID
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])

except Exception as err:
	print "Test 6 FAILED: Could not make POST Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 6 PASS: Succesfully Made POST Request to /Users"

#Making a GET Request
print "Making a GET Request for /Users..."
try:
	url = address + "/scene?name=yonas&scene_name=dog"
	h = httplib2.Http()
	resp, result = h.request(url, 'GET')
	obj = json.loads(result)
	ID=obj['scene']['name']
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])
except Exception as err:
	print "Test 2 FAILED: Could not make GET Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 2 PASS: Succesfully Made GET Request to /users"
###########################################################################
##                                                                       ##
##                        Files                                          ##
###########################################################################

#Making a POST Request
print "Making a POST request to /Scene..."
try:
	url = address + "/scene?name=yonas&scene_name=dog"
	request.add_header("Content-type", "application/x-www-form-urlencoded; charset=UTF-8")
	h = httplib2.Http()
	resp, result = h.request(url, 'POST')
	obj = json.loads(result)
	ID  = obj['sce']['id']
	Name =obj['sce']['name']
	print Name , ID
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])

except Exception as err:
	print "Test 6 FAILED: Could not make POST Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 6 PASS: Succesfully Made POST Request to /Users"

#Making a GET Request
print "Making a GET Request for /Users..."
try:
	url = address + "/scene?name=yonas&scene_name=dog"
	h = httplib2.Http()
	resp, result = h.request(url, 'GET')
	obj = json.loads(result)
	ID=obj['scene']['name']
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])
except Exception as err:
	print "Test 2 FAILED: Could not make GET Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 2 PASS: Succesfully Made GET Request to /users"


#Making a POST request for a file of type ['ply','png', 'jpg', 'jpeg', 'gif']
try:
	data=open('cold.png','rb')
	files = {'inputFile':data}
	url = address + "/upload?name=user1&scene_name=sun"
	res = requests.post(url=url,files=files)
	print res
	if resp['status'] != '200':
		raise Exception('Received an unsuccessful status code of %s' % resp['status'])

except Exception as err:
	print "Test 7 FAILED: Could not make POST Request to web server"
	print err.args
	sys.exit()
else:
	print "Test 7 PASS: Succesfully Made POST Request to /Users"
	
