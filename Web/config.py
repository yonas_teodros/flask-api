import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Auth:
    CLIENT_ID =('962404191932-8cej4mnt71fmskc2iqeqh7l8fh9ht91v.apps.googleusercontent.com')
    CLIENT_SECRET = 'tWBuQJGe2K4hHpf4_8rOxEQL'
    REDIRECT_URI = 'https://3dom.fbk.eu/projects'
    AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
    TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    USER_INFO = 'https://www.googleapis.com/userinfo/v2/me'


class Config:
    APP_NAME = "Test Google Login"
    SECRET_KEY = os.environ.get("SECRET_KEY") or "somethingsecret"


class DevConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, "test.db")


class ProdConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, "prod.db")


config = {
    "dev": DevConfig,
    "prod": ProdConfig,
    "default": DevConfig
}
