from flask import Flask, request, jsonify, render_template, redirect, url_for, send_from_directory,flash
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, User,Scene,Files
import os
from werkzeug.utils import secure_filename
from sqlalchemy import exists
#for oauth 
from flask import session as login_session
import random
import string
from oauth2client.client import flow_from_clientsecrets
from oauth2client.client import FlowExchangeError
import httplib2
import json
from flask import make_response
import requests

UPLOAD_FOLDER = '/home/ytefera/Api_Server/flask-api/flask-api/Web/uploads'
ALLOWED_EXTENSIONS = set(['ply','png','jpg', 'jpeg', 'gif'])

engine = create_engine('sqlite:///users.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

app = Flask(__name__)
CLIENT_ID=json.loads(open('client_secrets.json','r').read())['web']['client_id']
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/")
def indexfunction():
     return render_template('index.html')

@app.route("/admin")
def adminfunction():
    return render_template('admin.html')

# Create anti-forgery state token
@app.route('/login')
def showLogin():
    state = ''.join(random.choice(string.ascii_uppercase + string.digits)
                    for x in xrange(32))
    login_session['state'] = state
    return render_template('login.html',STATE=state)
	
#redirecting function
@app.route('/gconnect', methods=['POST'])
def gconnect():
    # Validate state token
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # Obtain authorization code
    code = request.data

    try:
        # Upgrade the authorization code into a credentials object
        oauth_flow = flow_from_clientsecrets('client_secrets.json', scope='')
        oauth_flow.redirect_uri = 'postmessage'
        credentials = oauth_flow.step2_exchange(code)
    except FlowExchangeError:
        response = make_response(
            json.dumps('Failed to upgrade the authorization code.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Check that the access token is valid.
    access_token = credentials.access_token
    url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
           % access_token)
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])
    # If there was an error in the access token info, abort.
    if result.get('error') is not None:
        response = make_response(json.dumps(result.get('error')), 500)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is used for the intended user.
    gplus_id = credentials.id_token['sub']
    if result['user_id'] != gplus_id:
        response = make_response(
            json.dumps("Token's user ID doesn't match given user ID."), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is valid for this app.
    if result['issued_to'] != CLIENT_ID:
        response = make_response(
            json.dumps("Token's client ID does not match app's."), 401)
        print "Token's client ID does not match app's."
        response.headers['Content-Type'] = 'application/json'
        return response

    stored_access_token = login_session.get('access_token')
    stored_gplus_id = login_session.get('gplus_id')
    if stored_access_token is not None and gplus_id == stored_gplus_id:
        response = make_response(json.dumps('Current user is already connected.'),
                                 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Store the access token in the session for later use.
    login_session['access_token'] = credentials.access_token
    login_session['gplus_id'] = gplus_id

    # Get user info
    userinfo_url = "https://www.googleapis.com/oauth2/v1/userinfo"
    params = {'access_token': credentials.access_token, 'alt': 'json'}
    answer = requests.get(userinfo_url, params=params)

    data = answer.json()

    login_session['username'] = data['name']
    login_session['picture'] = data['picture']
    login_session['email'] = data['email']

	# see if user exists, if it doesn't make a new one
    user_id = getUserID(data["email"])
    if not user_id:
        user_id = createUser(login_session)
    login_session['user_id'] = user_id

    output = ''
    output += '<h1>Welcome, '
    output += login_session['username']
    output += '!</h1>'
    output += '<img src="'
    output += login_session['picture']
    output += ' " style = "width: 300px; height: 300px;border-radius: 150px;-webkit-border-radius: 150px;-moz-border-radius: 150px;"> '
    flash("you are now logged in as %s" % login_session['username'])
    print "done!"
    return output


# User Helper Functions


def createUser(login_session):
    newUser = User(name=login_session['username'], email=login_session[
                   'email'], picture=login_session['picture'])
    session.add(newUser)
    session.commit()
    user = session.query(User).filter_by(email=login_session['email']).one()
    return user.id


def getUserInfo(user_id):
    user = session.query(User).filter_by(id=user_id).one()
    return user


def getUserID(email):
    try:
        user = session.query(User).filter_by(email=email).one()
        return user.id
    except:
        return None

@app.route('/gdisconnect')
def gdisconnect():
        # Only disconnect a connected user.
    access_token = login_session.get('access_token')
    if access_token is None:
        response = make_response(
           	json.dumps('Current user not connected.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    url = 'https://accounts.google.com/o/oauth2/revoke?token=%s' % access_token
    h = httplib2.Http()
    result = h.request(url, 'GET')[0]

    if result['status'] == '200':
        # Reset the user's sesson.
        del login_session['access_token']
        del login_session['gplus_id']
        del login_session['username']
        del login_session['email']
        del login_session['picture']

        response = make_response(json.dumps('Successfully disconnected.'), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
    else:
        # For whatever reason, the given token was invalid.
        response = make_response(
            json.dumps('Failed to revoke token for given user.', 400))
        response.headers['Content-Type'] = 'application/json'
        return response

@app.route("/users", methods = ['GET', 'POST'])
def usersFunction():
  if request.method == 'GET':
    return getAllUsers()
  elif request.method == 'POST':

    print "Making a New user"
    name = request.args.get('name', '')
    description = request.args.get('description', '')
    print name
    print description
    return makeANewUser(name, description)

# @app.route("/upload/<string:page_name>/", methods=['GET'])
# def load(page_name):
#     return render_template('%s.html' % page_name)


@app.route("/single_user", methods = ['GET', 'PUT', 'DELETE'])
def usersFunctionId():
  name = request.args.get('name', '')
  if request.method == 'GET':
     return getUser(name)
  elif request.method == 'PUT':
    description = request.args.get('description', '')
    return updateUser(name, description)
  elif request.method == 'DELETE':
    return deleteUser(name)

@app.route("/Project", methods = ['GET','POST'])
def scene():
    name = request.args.get('name', '')
    project_name=request.args.get('project_name', '')
    if request.method == 'GET':
       return getProject(name , project_name)
    elif request.method == 'POST':
        return makeANewProject(name , project_name)

@app.route("/upload", methods = ['POST'])
def upload():
    name = request.args.get('name', '')
    project_name=request.args.get('project_name', '')
    path = UPLOAD_FOLDER +'/'+ name + '/' + project_name
    file = request.files['inputFile']
    if session.query(exists().where(User.name == name)).scalar():
      if session.query(exists().where(Scene.name == project_name)).scalar():
        if session.query(exists().where(Files.name == file.filename)).scalar():
           return jsonify("afile with this name exist")
        else:
          if file and allowed_file(file.filename):
              #filename = secure_filename(file.filename)
              file.save(os.path.join(path, file.filename))
              scene = session.query(Scene).filter_by(name = project_name).one()
              newFile = Files(name=file.filename,sub_owner= scene)
              session.add(newFile)
              session.commit()
              return file.filename
      else:
        return jsonify("a project with this name dose not exist")
    else:
      return jsonify("there is no user with this name exist")

@app.route("/download/<path:filename>", methods = ['GET'])
def download(filename):
    name = request.args.get('name', '')
    project_name=request.args.get('project_name', '')
    if session.query(exists().where(User.name == name)).scalar():
        if session.query(exists().where(Scene.name == project_name)).scalar():
            if session.query(exists().where(Files.name == filename)).scalar():
                SCENE_FOLDER = UPLOAD_FOLDER + '/' + name + '/' + project_name
                app.config['SCENE_FOLDER'] = SCENE_FOLDER
                uploads = os.path.join(app.root_path, app.config['SCENE_FOLDER'])
                return send_from_directory(directory=uploads, filename=filename)
            else:
              return jsonify("a file with this name dose not exist")
        else:
          return jsonify("a project with this name dose not exist")
    else:
      return jsonify("there is no user with this name exist")



def getAllUsers():
  users = session.query(User).all()
  return jsonify(Users=[i.serialize for i in users])

def getUser(name):
  user = session.query(User).filter_by(name = name).one()
  return jsonify(user=user.serialize)

def makeANewUser(name,description):
  if session.query(exists().where(User.name == name)).scalar():
     return jsonify("user exists!")
  else:
      user = User(name = name, description = description)
      os.makedirs(UPLOAD_FOLDER + "/"+name)
      session.add(user)
      session.commit()
      return jsonify(user=user.serialize)

def updateUser(name, description):
  user = session.query(User).filter_by(name = name).one()
  #if not name:
  user.name = name
  #if not description:
  user.description = description
  session.add(user)
  session.commit()
  return jsonify("User Updated !")

def deleteUser(name):
  user = session.query(User).filter_by(name = name).one()
  session.delete(user)
  session.commit()
  return jsonify("User Removed !")

def getProject(name, project_name):
    if session.query(exists().where(Scene.name == project_name)).scalar():
       scene = session.query(Scene).filter_by(name = project_name).one()
       return jsonify(scene=scene.serialize)
    else:
      return jsonify("There is no Project with this name")
def makeANewProject(name , project_name):
  if session.query(exists().where(User.name == name)).scalar():
      if session.query(exists().where(Scene.name == project_name)).scalar():
         return jsonify("on going project with this name exists!")
      else:
          SCENE_FOLDER = UPLOAD_FOLDER + '/' + name + '/' + project_name
          app.config['SCENE_FOLDER'] = SCENE_FOLDER
          os.makedirs(SCENE_FOLDER)
          user = session.query(User).filter_by(name = name).one()
          sce=Scene(name=project_name,owner=user)
          session.add(sce)
          session.commit()
          return jsonify(sce=sce.serialize)
  else:
        return jsonify("There is no user with this name please create a user first!")

if __name__ == '__main__':
	app.secret_key = 'super_secret_key'
	app.debug = False
	app.run(host='localhost', port=5000)
