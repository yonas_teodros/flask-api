from sqlalchemy import Column, Integer, String, LargeBinary,ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship


Base = declarative_base()

class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key = True)
    name = Column(String(250), nullable=False)
    email = Column(String(250), nullable=False)
    picture = Column(String(250))
    scenes = relationship('Scene', backref='owner', lazy='dynamic')
    description = Column(String(250))
	    
    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
          'id': self.id,
           'name': self.name,
           'description' : self.description
           }

 
class Scene(Base):
    __tablename__ = 'scene'

    id = Column(Integer, primary_key = True)    
    name = Column(String(250))
    description = Column(String(250))
    user_id = Column(Integer,ForeignKey('user.id'))
    files = relationship('Files',backref='sub_owner',lazy='dynamic')
    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
          'id': self.id,
           'name': self.name,
           'description' : self.description
           }


class Files(Base):
    __tablename__ = 'files'
    
    id = Column(Integer, primary_key = True)
    name =Column(String(250))
    scene_id = Column(Integer,ForeignKey('scene.id'))
    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
          'id': self.id,
           'name': self.name,
           'description' : self.description
           }

engine = create_engine('sqlite:///users.db')
Base.metadata.create_all(engine)
